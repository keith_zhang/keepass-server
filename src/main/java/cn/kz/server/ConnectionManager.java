package cn.kz.server;

import io.netty.channel.ChannelHandlerContext;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by kz on 2017/3/5.
 */
public class ConnectionManager {
    private static Map<String, ChannelHandlerContext> map = new ConcurrentHashMap<String, ChannelHandlerContext>();

    private static ConnectionManager manager = new ConnectionManager();

    public static ConnectionManager getInstance() {
        return manager;
    }

    public void put(String key, ChannelHandlerContext ctx) {
        map.put(key, ctx);
    }

    public ChannelHandlerContext get(String key) {
        return map.get(key);
    }

    public void removeByCtx(ChannelHandlerContext ctx) {
        for (Map.Entry<String, ChannelHandlerContext> entry : map.entrySet()) {
            if (entry.getValue().equals(ctx)) {
                map.remove(entry.getKey());
                break;
            }
        }
    }
}
